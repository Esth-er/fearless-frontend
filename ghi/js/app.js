function createCard(name, description, pictureUrl, start, end, location) {
  return `
    <div class="col-sm-6 col-md-4 mb-1">
      <div class="shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">${start} - ${end}</small>
        </div>
      </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const location = details.conference.location.name;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const start = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`;
          const endDate = new Date(details.conference.ends);
          const end = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`;
          const html = createCard(name, description, pictureUrl, start, end, location);

          const column = document.querySelector('.row');
          column.innerHTML += html;
          let imageTag = document.querySelector('.card-img-top');
          imageTag.src = details.conference.location.picture_url;
        }
      }

    }
  } catch (e) {
    const error = console.error(e);
    return `
    <div>
      ${error}
      </div>
    `;
    // Figure out what to do if an error is raised
  }

});
